let myPokemon = {
	name: 'Pikachu',
	level: 3, 
	health: 100,
	attack: 50,
	tackle: function() {
		console.log("This pokemon tackled target Pokemon")
	},
	faint: function() {
		console.log("Pokemon fainted")
	}
}

console.log(myPokemon);

//we will use object constructor to create another pokemon

function Pokemon(name, level) {
	//properties
	this.name = name;
	this.level = level;
	this.health = 5 * level;
	this.attack = level;
	//methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name);
		target.health=target.health-this.attack;
		console.log(target.name + "'s health is now reduced to " + target.health);
			if(target.health<=5){
				target.faint
			}
	},
	this.faint = function() {
			console.log(this.name + " fainted")
		
	}
}

//creates new instance of the "Pokemon"  object each with their unique properties
let pikachu = new Pokemon("Pikachu", 16);
let squirtle = new Pokemon("Squirtle", 8);

pikachu.tackle(squirtle)

//24 - 16 = 8 health

/*Activity:
1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
(target.health - this.attack)

2.) If health is less than or equal to 5, invoke faint function*/

function PokemonOne(name, level) {
	//properties
	this.name = name;
	this.level = level;
	this.health = 5 * level;
	this.attack = 2 * level;
	//methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name);
		target.health=target.health-this.attack;
		console.log(target.name + "'s health is now reduced to " + target.health);
			if(target.health<=5){
				target.faint
			}
	},
	this.faint = function() {
			console.log(this.name + " fainted")
		
	}
}
let bulbasaur = new Pokemon("Bulbasaur", 4);
squirtle.tackle(bulbasaur)
squirtle.tackle(bulbasaur)
squirtle.faint()